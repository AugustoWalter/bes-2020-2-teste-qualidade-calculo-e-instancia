package br.ucsal.testequalidade.calculoetui;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import br.ucsal.testequalidade.CalculoETUI;

public class CalculoETUITest {

	private static CalculoEHelperStub calculoEHelperStub;
	private static TuiUtilMock tuiUtilMock;
	private static CalculoETUI calculoETUI;

	@BeforeAll
	public static void setup() {
		calculoEHelperStub = new CalculoEHelperStub();
		tuiUtilMock = new TuiUtilMock();
		calculoETUI = new CalculoETUI(calculoEHelperStub, tuiUtilMock);

	}

	@Test
	public void obterNCalcularExibirE2() {
		calculoETUI.obterNCalcularExibirE();
		tuiUtilMock.verificarChamadaExibbirMensagem("E(2)=2.5");



	}

	//obterNCalcularExibirE2 é um método Command (um método que NÃO tem retorno)!


}
