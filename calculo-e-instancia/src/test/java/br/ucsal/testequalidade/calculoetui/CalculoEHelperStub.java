package br.ucsal.testequalidade.calculoetui;

import br.ucsal.testequalidade.CalculoEHelper;
import br.ucsal.testequalidade.FatorialHelper;

public class CalculoEHelperStub extends CalculoEHelper  {

	public CalculoEHelperStub(FatorialHelper fatorialHelper) {
		super(fatorialHelper);

	}

	public CalculoEHelperStub() {
		super(null);
	}
	@Override
	public Double calcularE(Integer n) { 
		switch (n) {
		case 2:
			return 2.5;
		default:
			throw new RuntimeException("calcularE(" + n + ") não definido em CalculoEHelperStub");
		}





	}





}
